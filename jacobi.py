from math import gcd


def jacobi_symbol(a, n):
    if n < 0 or not n % 2:
        raise ValueError("n должно быть нечётным положительным целым числом")
    if a < 0 or a > n:
        a %= n
    if not a:
        return int(n == 1)
    if n == 1 or a == 1:
        return 1
    if gcd(a, n) != 1:
        return 0

    j = 1
    while a != 0:
        while a % 2 == 0 and a > 0:
            a >>= 1
            if n % 8 in [3, 5]:
                j = -j
        a, n = n, a
        if a % 4 == n % 4 == 3:
            j = -j
        a %= n
    return j


def main():
    n = int(input('Введите n: '))
    a = int(input('Введите a: '))
    print(f'Символ Якоби: {jacobi_symbol(a, n)}')


if __name__ == '__main__':
    main()
