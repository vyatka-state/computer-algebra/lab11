from math import sqrt


def isprime(n):
    for i in range(2, int(sqrt(n))):
        if n % i == 0:
            return False
    return True


def factorize(n):
    factors = []

    p = 2
    while True:
        while n % p == 0 and n > 0:
            factors.append(p)
            n //= p
        p += 1
        if p > n / p:
            break
    if n > 1:
        factors.append(n)
    return factors


def legendre_symbol(a, p):
    if not isprime(p) or p == 2:
        raise ValueError("p должно быть нечётным простым")
    if a >= p or a < 0:
        return legendre_symbol(a % p, p)
    elif a == 0 or a == 1:
        return a
    elif a == 2:
        if p % 8 == 1 or p % 8 == 7:
            return 1
        else:
            return -1
    elif a == p - 1:
        if p % 4 == 1:
            return 1
        else:
            return -1
    elif not isprime(int(a)):
        factors = factorize(a)
        product = 1
        for pi in factors:
            product *= legendre_symbol(pi, p)
        return product
    else:
        if ((p - 1) // 2) % 2 == 0 or ((a - 1) // 2) % 2 == 0:
            return legendre_symbol(p, a)
        else:
            return -1 * legendre_symbol(p, a)


def main():
    a = int(input('Введите a: '))
    p = int(input('Введите p: '))
    print(f'Символ Лежандра: {legendre_symbol(a, p)}')


if __name__ == '__main__':
    main()
