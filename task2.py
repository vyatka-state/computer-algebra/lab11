from jacobi import jacobi_symbol


def main():
    m = int(input('Введите модуль: '))
    if m < 2:
        print('Модуль должен быть больше 1')
        return

    deductions = []
    not_deductions = []
    for i in range(m):
        current_jacobi = jacobi_symbol(i + 1, m)
        if current_jacobi == 1:
            deductions.append(i + 1)
        elif current_jacobi == -1:
            not_deductions.append(i + 1)

    print(f'Вычеты: {deductions}')
    print(f'Невычеты: {not_deductions}')


if __name__ == '__main__':
    main()
